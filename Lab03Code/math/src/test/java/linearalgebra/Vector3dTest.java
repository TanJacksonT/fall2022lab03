package linearalgebra;


import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class Vector3dTest {

    private static Vector3d newObj = new Vector3d (1,2,3);
    private static Vector3d secondObj = new Vector3d(2,3,4);
    // this test will fail
    
    @Test
    public void testGetX() { 
        assertEquals(1,newObj.getX(),0);
    }
    
    @Test
    public void testMagnitudeGood() {
        assertEquals(3.7416573867739413,newObj.magnitude(),1);
    }

    @Test 
    public void testDotProduct() {
        assertEquals(20.0,newObj.dotProduct(secondObj),0);
    }

    @Test
    public void testaddX() {
        assertEquals(3,(newObj.getX() + secondObj.getX()),0);
    }

    @Test
    public void testaddY() {
        assertEquals(5,(newObj.getY() + secondObj.getY()),0);
    }

    @Test
    public void testaddZ() {
        assertEquals(7,(newObj.getZ() + secondObj.getZ()),0);
    }

}

