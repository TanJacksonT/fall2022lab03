package linearalgebra;

public class Vector3d  {        
    private double x;
    private double y; 
    private double z;

    

    public Vector3d(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
           
    }
    
    public double getX () {
        return this.x;
    }

    public double getY () {
        return this.y;
    }

    public double getZ () {
        return this.z;
    }

    public double magnitude() {
        return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2) + Math.pow(this.z, 2));
    }

    public double dotProduct(Vector3d newVector3d) {
        double total = 0;
        total = (this.x*newVector3d.x) + (this.y*newVector3d.y) + (this.z*newVector3d.z);
        return total;
    }

    public Vector3d add(Vector3d secondVector3d) { 
        Vector3d newValue = new Vector3d(this.x,this.y,this.z);
        newValue.x = this.x+secondVector3d.x;
        newValue.y = this.y+secondVector3d.y;
        newValue.z = this.z+secondVector3d.z;
        return newValue;
    }

    public String toString() { 
        return "x value is: " + x + " y value is: " + y + " z value is: " + z;
    }
}

